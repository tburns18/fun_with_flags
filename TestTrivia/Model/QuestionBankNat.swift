//
//  QuestionBankAdv.swift
//  TestTrivia
//
//  Created by sadmin on 12/1/19.
//  Copyright © 2019 Group 17. All rights reserved.
//

import Foundation
class QuestionBankNat {
    var listNat = [QuestionNat]()
    init() {
        listNat.append(QuestionNat(image: "af", questionText: "Name the country of this flag.", choiceA: randAns(), choiceB: "Afghanistan", choiceC: randAns(), choiceD: randAns(), answer: 2))
        listNat.append(QuestionNat(image: "alb", questionText: "Name the country of this flag.", choiceA: randAns(), choiceB: randAns(), choiceC: "Albania", choiceD: randAns(), answer: 3))
        listNat.append(QuestionNat(image: "at", questionText: "Name the country of this flag.", choiceA: randAns(), choiceB: "Austria", choiceC: randAns(), choiceD: randAns(), answer: 2))
        listNat.append(QuestionNat(image: "au", questionText: "Name the country of this flag.", choiceA: "Australia", choiceB: randAns(), choiceC: randAns(), choiceD: randAns(), answer: 1))
        listNat.append(QuestionNat(image: "bh", questionText: "Name the country of this flag.", choiceA: randAns(), choiceB: randAns(), choiceC: randAns(), choiceD: "Bahrain", answer: 4))
        listNat.append(QuestionNat(image: "bd", questionText: "Name the country of this flag.", choiceA: randAns(), choiceB: "Bangladesh", choiceC: randAns(), choiceD: randAns(), answer: 2))
        listNat.append(QuestionNat(image: "bzb", questionText: "Name the country of this flag.", choiceA: randAns(), choiceB: randAns(), choiceC: "Belize", choiceD: randAns(), answer: 3))
        listNat.append(QuestionNat(image: "cr", questionText: "Name the country of this flag.", choiceA: "Costa Rica", choiceB: randAns(), choiceC: randAns(), choiceD: randAns(), answer: 1))
        listNat.append(QuestionNat(image: "de", questionText: "Name the country of this flag.", choiceA: randAns(), choiceB: "Germany", choiceC: randAns(), choiceD: randAns(), answer: 2))
        listNat.append(QuestionNat(image: "fr", questionText: "Name the country of this flag.", choiceA: randAns(), choiceB: randAns(), choiceC: randAns(), choiceD: "France", answer: 4))
        listNat.append(QuestionNat(image: "gb", questionText: "Name the country of this flag.", choiceA: "Great Britain", choiceB: randAns(), choiceC: randAns(), choiceD: randAns(), answer: 1))
        listNat.append(QuestionNat(image: "bw", questionText: "Name the country of this flag.", choiceA: randAns(), choiceB: randAns(), choiceC: "Botswana", choiceD: randAns(), answer: 3))
        listNat.append(QuestionNat(image: "cn", questionText: "Name the country of this flag.", choiceA: "China", choiceB: randAns(), choiceC: randAns(), choiceD: randAns(), answer: 1))
        listNat.append(QuestionNat(image: "cu", questionText: "Name the country of this flag.", choiceA: randAns(), choiceB: "Cuba", choiceC: randAns(), choiceD: randAns(), answer: 2))
        listNat.append(QuestionNat(image: "cz", questionText: "Name the country of this flag.", choiceA: randAns(), choiceB: randAns(), choiceC: randAns(), choiceD: "Czech Republic", answer: 4))
    }
    var number: Int = 0
    func randAns() -> String{
        let answers: [String] = ["Afghanistan",
        "Albania",
        "Armenia",
        "Angola",
        "Austria",
        "Australia",
        "Bahrain",
        "Bangladesh",
        "Barbados",
        "Belarus",
        "Belgium",
        "Belize",
        "Bolivia",
        "Brazil",
        "Canada",
        "Chile",
        "Colombia",
        "Costa Rica",
        "Croatia",
        "Cuba",
        "Denmark",
        "Egypt",
        "Estonia",
        "Finland",
        "France",
        "Germany",
        "Ghana",
        "Greece",
        "Haiti",
        "Hungary",
        "India",
        "Indonesia",
        "Iran",
        "Iraq",
        "Ireland",
        "Israel",
        "Italy",
        "Jamaica",
        "Japan",
        "Kenya",
        "Kuwait",
        "Lebanon",
        "Madagascar",
        "Mali",
        "Mexico",
        "Norway",
        "Pakistan",
        "Qatar",
        "Russia",
        "South Korea",
        "Spain",
        "Sweden",
        "Switzerland",
        "Thailand",
        "Ukraine",
        "United States"]
        number = Int.random(in: 0 ..< answers.count-1)
        let randomAns = answers[number]
        return randomAns
    }

}
