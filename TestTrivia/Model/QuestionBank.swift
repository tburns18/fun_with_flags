//
//  QuestionBank.swift
//  TestTrivia
//
//  Created by sadmin on 11/24/19.
//  Copyright © 2019 Group 17. All rights reserved.
//

import Foundation

class QuestionBank {
    var list = [Question]()
    
    init() {
        list.append(Question(image: "al", questionText: "Name the state of this flag.", choiceA: randAns(), choiceB: "Alabama", choiceC: randAns(), choiceD: randAns(), answer: 2))
        list.append(Question(image: "ak", questionText: "Name the state of this flag.", choiceA: randAns(), choiceB: randAns(), choiceC: randAns(), choiceD: "Alaska", answer: 4))
        list.append(Question(image: "ar", questionText: "Name the state of this flag.", choiceA: "Arkansas", choiceB: randAns(), choiceC: randAns(), choiceD: randAns(), answer: 1))
        list.append(Question(image: "az", questionText: "Name the state of this flag.", choiceA: randAns(), choiceB: randAns(), choiceC: "Arizona", choiceD: randAns(), answer: 3))
        list.append(Question(image: "ca", questionText: "Name the state of this flag.", choiceA: randAns(), choiceB: randAns(), choiceC: randAns(), choiceD: "California", answer: 4))
        list.append(Question(image: "co", questionText: "Name the state of this flag.", choiceA: "Colorado", choiceB: randAns(), choiceC: randAns(), choiceD: randAns(), answer: 1))
        list.append(Question(image: "ct", questionText: "Name the state of this flag.", choiceA: randAns(), choiceB: randAns(), choiceC: "Connecticut", choiceD: randAns(), answer: 3))
        list.append(Question(image: "de", questionText: "Name the state of this flag.", choiceA: randAns(), choiceB: "Delaware", choiceC: randAns(), choiceD: randAns(), answer: 2))
        list.append(Question(image: "ga", questionText: "Name the state of this flag.", choiceA: randAns(), choiceB: randAns(), choiceC: randAns(), choiceD: "Georgia", answer: 4))


        //Level 2
        
        list.append(Question(image: "america", questionText: "Which flag is a red St. Andrew's cross on a white square?",choiceA: randAns(), choiceB: randAns(), choiceC: randAns(), choiceD: "Alabama", answer: 4))
        list.append(Question(image: "america", questionText: "Which flag has the Big Dipper?",choiceA: "Alaska", choiceB: randAns(), choiceC: randAns(), choiceD: randAns(), answer: 1))
        list.append(Question(image: "america", questionText: "Which is the only flag with a red star?",choiceA: randAns(), choiceB: randAns(), choiceC: "California", choiceD: "Alabama", answer: 3))
        list.append(Question(image: "america", questionText: "Which flag has the words 'In God We Trust'?",choiceA: randAns(), choiceB: randAns(), choiceC: randAns(), choiceD: "Florida", answer: 4))
        list.append(Question(image: "america", questionText: "Which state changed its flag in 2003? ",choiceA: randAns(), choiceB: "Georgia", choiceC: randAns(), choiceD: randAns(), answer: 2))
        list.append(Question(image: "america", questionText: "Which flag has the Union Jack in the canton? ",choiceA: randAns(), choiceB: randAns(), choiceC: randAns(), choiceD: "Hawaii", answer: 4))
        list.append(Question(image: "america", questionText: "Which is the only flag with the word 'Commonwealth' on it?",choiceA: "Kentucky", choiceB: randAns(), choiceC: randAns(), choiceD: randAns(), answer: 1))
        list.append(Question(image: "america", questionText: "Which is the only flag with Roman numerals on it?",choiceA: randAns(), choiceB: randAns(), choiceC: "Missouri", choiceD: randAns(), answer: 4))
    }

    
    var number = 0
    
    func randAns() -> String{
        let answers: [String] = ["Alabama",
        "Alaska",
        "Arizona",
        "Arkansas",
        "California",
        "Colorado",
        "Connecticut",
        "Delaware",
        "Florida",
        "Georgia",
        "Hawaii",
        "Idaho",
        "Illinois",
        "Indiana",
        "Iowa",
        "Kansas",
        "Kentucky",
        "Louisiana",
        "Maine",
        "Maryland",
        "Massachusetts",
        "Michigan",
        "Minnesota",
        "Mississippi",
        "Missouri",
        "Montana",
        "Nebraska",
        "Nevada",
        "New Hampshire",
        "New Jersey",
        "New Mexico",
        "New York",
        "North Carolina",
        "North Dakota",
        "Ohio",
        "Oklahoma",
        "Oregon",
        "Pennsylvania",
        "Rhode Island",
        "South Carolina",
        "South Dakota",
        "Tennessee",
        "Texas",
        "Utah",
        "Vermont",
        "Virginia",
        "Washington",
        "West Virginia",
        "Wisconsin",
        "Wyoming"]
        number = Int.random(in: 0 ..< 50)
        let randomAns = answers[number]
        return randomAns
    }
    
    }

