//
//  QuestionAdv.swift
//  TestTrivia
//
//  Created by sadmin on 12/4/19.
//  Copyright © 2019 Group 17. All rights reserved.
//

import Foundation

class QuestionNat {
    let questionImage: String
    let question: String
    let optionA: String
    let optionB: String
    let optionC: String
    let optionD: String
    let correctAnswer: Int
    
    init(image: String, questionText: String, choiceA: String, choiceB: String, choiceC: String, choiceD: String, answer: Int) {
        questionImage = image
        question = questionText
        optionA = choiceA
        optionB = choiceB
        optionC = choiceC
        optionD = choiceD
        correctAnswer = answer
    }
}
