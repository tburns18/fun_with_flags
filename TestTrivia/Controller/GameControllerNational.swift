//
//  GameControllerNational.swift
//  TestTrivia
//
//  Created by sadmin on 12/9/19.
//  Copyright © 2019 Group 17. All rights reserved.
//

import Foundation
import UIKit

class GameControllerNational: UIViewController{
    
    @IBOutlet weak var questionCounter: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var flagView: UIImageView!
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var optionA: UIButton!
    @IBOutlet weak var optionB: UIButton!
    @IBOutlet weak var optionC: UIButton!
    @IBOutlet weak var optionD: UIButton!
    
    @IBOutlet weak var restart: UIButton!
    
    let allNatQuestions = QuestionBankNat()
    //let nextQuestions = QuestionBankAdv()
    var questionNumber: Int = 0
    var score: Int = 0
    var selectedAnswer: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        updateQuestion()
        updateUI()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func restart(_ sender: Any) {
        reallyRestart()
        }
        func reallyRestart(){
            let alert = UIAlertController(title: "Restart", message: "Do you really want to restart?", preferredStyle: .alert)
            let restartAction = UIAlertAction(title: "Restart", style: .default, handler: {action in self.restartQuiz()})
            let cancel = UIAlertAction(title: "Cancel", style: .default, handler: {action in self.cancel()})
            
            alert.addAction(restartAction)
            alert.addAction(cancel)
            present(alert, animated: true, completion: nil)
    }
    
    @IBAction func answerPressed(_ sender: UIButton) {
           condition: if sender.tag == selectedAnswer {
                      print("Correct")
                      score += 1
                      break condition
                  }else{
                      print("Incorrect")
                      score += 0
                      break condition
                  }
               
               questionNumber += 1
               updateQuestion()
               
       }
    
    func updateQuestion(){
        
        if questionNumber < allNatQuestions.listNat.count-1{
            flagView.image = UIImage(named:(allNatQuestions.listNat[questionNumber].questionImage))
            questionLabel.text = allNatQuestions.listNat[questionNumber].question
            optionA.setTitle(allNatQuestions.listNat[questionNumber].optionA, for: UIControl.State.normal)
            optionB.setTitle(allNatQuestions.listNat[questionNumber].optionB, for: UIControl.State.normal)
            optionC.setTitle(allNatQuestions.listNat[questionNumber].optionC, for: UIControl.State.normal)
            optionD.setTitle(allNatQuestions.listNat[questionNumber].optionD, for: UIControl.State.normal)
            selectedAnswer = allNatQuestions.listNat[questionNumber].correctAnswer
        }
        else if questionNumber == 15{
                let alert = UIAlertController(title: "Complete!", message: "You earned \(score) points!", preferredStyle: .alert)
                let restartAction = UIAlertAction(title: "Restart", style: .default, handler: {action in self.restartQuiz()})
                let gameView = UIAlertAction(title: "Home", style: .default, handler: {action in self.goHome()})
                alert.addAction(restartAction)
                alert.addAction(gameView)
                present(alert, animated: true, completion: nil)
                
            }else{
                let alert = UIAlertController(title: "Try Again", message: "It looks like you might need to practice again! Do you want to start over?", preferredStyle: .alert)
                let restartAction = UIAlertAction(title: "Restart", style: .default, handler: {action in self.restartQuiz()})
                let gameView = UIAlertAction(title: "Home", style: .default, handler: {action in self.restartQuiz()})
                alert.addAction(restartAction)
                alert.addAction(gameView)
                present(alert, animated: true, completion: nil)
            }
        
        updateUI()
        }
        
        func updateUI(){
            scoreLabel.text = "Score: \(score)"
            questionCounter.text = "\(questionNumber + 1)/\(allNatQuestions.listNat.count+1)"
            progressView.frame.size.width = (view.frame.size.width / CGFloat(allNatQuestions.listNat.count)) * CGFloat(questionNumber)
        }
        
        func restartQuiz(){
            score = 0
            questionNumber = 0
            updateQuestion()
        }
        func cancel(){
            
        }
        func goHome(){
            
        }
            
        }

