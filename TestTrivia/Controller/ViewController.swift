//
//  ViewController.swift
//  TestTrivia
//
//  Created by Taylor Cobb on 11/18/19.
//  Copyright © 2019 Group 17. All rights reserved.
//

import UIKit
import FirebaseUI

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func loginTapped(_ sender: UIButton) {
        
        let authUI = FUIAuth.defaultAuthUI()
        
        guard authUI != nil else {
            return
        }
        
        authUI?.delegate = self
        authUI?.providers = [FUIEmailAuth()]
        
        let authViewController = authUI?.authViewController()
        
        present(authViewController!, animated: true, completion: nil)
    }
    
}



extension ViewController: FUIAuthDelegate {
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        
        if error != nil {
            return
        }
        performSegue(withIdentifier: "startGame", sender: self)
    }
}

